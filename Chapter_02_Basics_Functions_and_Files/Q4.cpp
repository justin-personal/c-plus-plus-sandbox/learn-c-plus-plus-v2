#include <iostream>

int doubleNumber(int num)
{
    return 2 * num;
}

int main( void )
{
    int num;
    std::cout << "Input number to double: ";
    std::cin >> num;

    std::cout << doubleNumber(num) << std::endl;
    return 0;
}
