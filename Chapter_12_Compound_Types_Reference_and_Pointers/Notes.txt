12.1 - Introduction to compound data types:
    Definitions:

12.2 - Value categories (lvalues and rvalues):
    Definitions:

12.3 - Lvalue references:
    Definitions:

12.4 - Lvalue references to const:
    Definitions:

12.5 - Pass by lvalue reference:
    Definitions:

12.6 - Pass by const lvalue reference:
    Definitions:

12.7 - Introduction to pointers:
    Definitions:

12.8 - Null pointers:
    Definitions:

12.9 - Pointers and const:
    Definitions:

12.10 - Pass by address:
    Definitions:

12.11 - Pass by address (part 2):
    Definitions:

12.12 - Return by reference and return by address:
    Definitions:

12.13 - In and out parameters:
    Definitions:

12.14 - Type deduction with pointers, references, and const:
    Definitions:

12.15 - std::optional:
    Definitions:

12.x  - Chapter 12 summary and quiz:
    Definitions:
