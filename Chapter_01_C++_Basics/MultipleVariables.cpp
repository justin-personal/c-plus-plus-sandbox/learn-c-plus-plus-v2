#include <iostream>

int main() {

    int a = 2;
    int b = 4;
    int c = 6, d = 8;
    int e = 4;
    int f {4};
    int g {4};

    std::cout << "a: " << a << std::endl;
    std::cout << "b: " << b << std::endl;
    std::cout << "c: " << c << std::endl;
    std::cout << "d: " << d << std::endl;
    std::cout << "e: " << e << std::endl;
    std::cout << "f: " << f << std::endl;
    std::cout << "g: " << g << std::endl;

    return 0;
}
