27.1 - The need for exceptions:
    Definitions:

27.2 - Basic exception handling:
    Definitions:

27.3 - Exceptions, functions, and stack unwinding:
    Definitions:

27.4 - Uncaught exceptions and catch-all handlers:
    Definitions:

27.5 - Exceptions, classes, and inheritance:
    Definitions:

27.6 - Rethrowing exceptions:
    Definitions:

27.7 - Function try blocks:
    Definitions:

27.8 - Exception dangers and downsides:
    Definitions:

27.9 - Exception specifications and noexcept:
    Definitions:

27.10 - std::move_if_noexcept:
    Definitions:

27.x - Chapter 27 summary and quiz:
    Definitions:
